import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusPipe } from './pipes/status.pipe';
import { TimeFormatPipe } from './pipes/time-format.pipe';

@NgModule({
  declarations: [StatusPipe, TimeFormatPipe],
  exports: [ StatusPipe, TimeFormatPipe ],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
