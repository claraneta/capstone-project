import { Component, OnInit, ViewChild } from "@angular/core";
import { Events, NavController, ToastController } from "@ionic/angular";
import { VehicleService } from "../../services/vehicle.service";
import { RouteService } from "../../services/route.service";
import { findWhere } from "underscore";
import { GoogleMap, MapInfoWindow, MapPolyline } from "@angular/google-maps";
import { isNumber } from "util";
import { HistoryService } from "../../services/history.service";
import { GeocodingService } from "../../services/geocoding.service";
import { GeolocationService } from "../../services/geolocation.service";

@Component({
  selector: "app-add-puv",
  templateUrl: "./add-puv.page.html",
  styleUrls: ["./add-puv.page.scss"]
})
export class AddPuvPage implements OnInit {
  @ViewChild(GoogleMap, { static: false }) map: GoogleMap;
  @ViewChild(MapInfoWindow, { static: false }) infoWindow: MapInfoWindow;
  @ViewChild(MapPolyline, { static: false }) poly: MapPolyline;

  public status: String = "";
  public type: String = "";
  public routes: Array<any> = [];
  public code: String = "";
  public selected_routes: Array<any> = [];
  public vehicles: Array<any> = [];
  public marker_positions: google.maps.LatLngLiteral[] = [];
  public marker_options = { draggable: false, title: "" };
  public geocoder = new google.maps.Geocoder();
  public paths: google.maps.MVCArray<google.maps.LatLngLiteral[]>;
  public poly_options = {
    strokeColor: "#FF0000",
    strokeOpacity: 1.0,
    strokeWeight: 3,
    map: this.map
  };

  today: number = Date.now();
  public show = true;
  public isDisabled = true;
  public vehicle_routes: Array<any> = [];
  public add_route: Array<any> = [];
  constructor(
    private events: Events,
    private nav: NavController,
    private vehicleService: VehicleService,
    private routeService: RouteService,
    private toast: ToastController,
    private historyService: HistoryService,
    private geocoderService: GeocodingService,
    private geolocationService: GeolocationService
  ) {}

  ngOnInit() {
    setTimeout(() => {
      this.initMapConfiguration();
    });
    this.events.publish("has_menu", true);
    this.events.publish("has_header", true);
    // this.initRoutes();
    this.initVehicles();
    setInterval(() => {
      this.today = Date.now();
    }, 1);
  }

  async initMapConfiguration() {
    if (navigator) {
      navigator.geolocation.getCurrentPosition(pos => {
        let location = {
          latitude: pos.coords.latitude,
          longitude: pos.coords.longitude
        };
        let position = {
          lat: location.latitude,
          lng: location.longitude
        };
        this.map.options = {
          mapTypeControl: true,
          scaleControl: true,
          zoomControl: true,
          fullscreenControl: false,
          streetViewControl: false,
          zoom: 15
        };
        this.map.center = { ...position };
      });
    }
  }

  // async initRoutes() {
  //   let result: any = await this.routeService.Listing();
  //   if (!result.error && result.data) {
  //     this.routes = result.data;
  //   } else {
  //     this.toastMessageError("Could not load routes");
  //   }
  // }

  async initVehicles() {
    let result: any = await this.vehicleService.Listing();
    this.vehicles = result.data;
  }

  async submit() {
    this.routes = this.marker_positions;
    this.code = this.code.toUpperCase();
    if (this.validateFields() && this.routes.length > 9) {
      let vehicle = findWhere(this.vehicles, { code: this.code });
      if (this.type === "Bus") {
        this.code = "";
        let _routes: Array<any> = [];
        for (let index = 0; index < this.add_route.length; index++) {
          let element = this.add_route[index];
          let result: any = await this.routeService.Save(element[0]);
          if (!result.error && result.data) {
            _routes.push(result.data._id);
          } else {
            this.toastMessageError(result.message);
          }
        }
        if (_routes.length > 0) {
          let data = {
            type: this.type,
            code: this.code,
            update_status: this.status,
            routes: _routes
          };
          let result: any = await this.vehicleService.Save(data);
          if (!result.error && result.data) {
            this.toastMessageSuccess(result.message);
            let _data = {
              action_type: "Add-PUV",
              message: `Bus ${result.message}`,
              date: this.today
            };
            let history = await this.historyService.Save(_data);
            this.nav.navigateRoot("puv");
          } else {
            this.toastMessageError(result.message);
            this.clearAllFields();
          }
        }
        console.log("Done");
      } else {
        if (vehicle) {
          this.toastMessageError("This jeepney code already exists");
          this.code = "";
        } else {
          let letters = /^[A-Za-z]+$/;
          if (
            this.code.length === 3 &&
            !isNaN(parseInt(this.code[0])) &&
            !isNaN(parseInt(this.code[1])) &&
            this.code[2].match(letters)
          ) {
            let _routes: Array<any> = [];
            for (let index = 0; index < this.add_route.length; index++) {
              let element = this.add_route[index];
              let result: any = await this.routeService.Save(element[0]);
              if (!result.error && result.data) {
                _routes.push(result.data._id);
              } else {
                this.toastMessageError(result.message);
              }
            }

            if (_routes.length > 0) {
              let data = {
                type: this.type,
                code: this.code,
                update_status: this.status,
                routes: _routes
              };
              let result: any = await this.vehicleService.Save(data);
              if (!result.error && result.data) {
                this.toastMessageSuccess(result.message);
                let _data = {
                  action_type: "Add-PUV",
                  message: `${data.code} ${result.message}`,
                  date: this.today
                };
                let history = await this.historyService.Save(_data);
                this.nav.navigateRoot("puv");
              } else {
                this.toastMessageError(result.message);
                this.clearAllFields();
              }
            }
            console.log("Done");
          } else {
            this.code = "";
            this.toastMessageError("Invalid jeepney code");
          }
        }
      }
    } else {
      this.toastMessageError(
        "Please populate all fields or set the route of vehicle at least ten marker positions"
      );
    }
  }

  async toastMessageSuccess(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: "success"
    });
    return toast.present();
  }
  async toastMessageError(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: "danger"
    });
    return toast.present();
  }

  clearAllFields() {
    this.type = "";
    this.code = "";
    this.routes = [];
    this.status = "";
  }

  validateFields() {
    if (this.type === "Bus" && this.status) {
      return true;
    } else {
      if (this.type === "Jeep" && this.code && this.status) {
        return true;
      } else {
        return false;
      }
    }
  }

  async addMarker(event) {
    let position = event.latLng.toJSON();
    this.marker_positions.push(position);
    this.marker_options.title = "Route #" + this.marker_positions.length;
    let route: any;
    let _landmark: Array<any> = [];
    let _routes: Array<any> = [];
    this.geocoder.geocode({ location: event.latLng.toJSON() }, function(
      results,
      status
    ) {
      if (status === "OK") {
        if (results[0]) {
          route = event.latLng.toJSON();
          route.landmark = results[0].formatted_address;
          let landmark = results[0].formatted_address;
          _landmark.push({ landmark });
          _routes.push(route);
        } else {
          window.alert("No results found");
        }
      } else {
        window.alert("Geocoder failed due to: " + status);
      }
    });
    this.add_route.push(_routes);
    //create a variable that will contain the markerpositions and add property "landmark"

    //draw the line or the poly need to be fixed ASAP
    this.drawRoute(event.latLng);
  }

  removeMarkers() {
    if (this.marker_positions.length == 0) {
      this.toastMessageError("Nothing to clear");
    } else {
      this.poly.getPath().clear();
      this.marker_positions.length = 0;
    }
  }

  openInfoWindow(marker) {
    this.infoWindow.open(marker);
  }

  async asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }

  drawRoute(latlng) {
    const service = new google.maps.DirectionsService();
    const path = this.poly.getPath();
    let self = this;
    if (path.getLength() === 0) {
      path.push(latlng);
    } else {
      service.route(
        {
          origin: path.getAt(path.getLength() - 1),
          destination: latlng,
          travelMode: google.maps.TravelMode.TRANSIT
        },

        function(result, status) {
          console.log(result);
          if (status == google.maps.DirectionsStatus.OK) {
            for (
              var i = 0, len = result.routes[0].overview_path.length;
              i < len;
              i++
            ) {
              path.push(result.routes[0].overview_path[i]);
              // this.paths = path.push(result.routes[0].overview_path[i]);
            }
          }
        }
      );
    }
  }
}
