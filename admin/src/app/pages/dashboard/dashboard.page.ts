import { Component, OnInit } from '@angular/core';
import { Events } from '@ionic/angular';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})

export class DashboardPage implements OnInit {
  
  constructor(
    private events: Events
  ) { }

  ngOnInit() {
    this.events.publish("has_menu", true);
    this.events.publish("has_header", true);
  }

}