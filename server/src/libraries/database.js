import mongoose from 'mongoose';

//connect with db
const url = 'mongodb://localhost:27017/capyot';
mongoose.connect(url, { useNewUrlParser: true });
mongoose.connection.on('connected', () => {
    console.log(`Database connected ${url}`);
});

mongoose.connection.on('error', err => {
    console.log('error while connecting to mongodb', err);
});