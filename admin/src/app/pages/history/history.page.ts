import { Component, OnInit } from "@angular/core";
import { Events, ToastController } from "@ionic/angular";
import { HistoryService } from "../../services/history.service";
import { PagerService } from "../../services/pager.service";
import * as moment from 'moment';

@Component({
  selector: "app-history",
  templateUrl: "./history.page.html",
  styleUrls: ["./history.page.scss"]
})
export class HistoryPage implements OnInit {
  p: number = 1;
  constructor(
    private pagerService: PagerService,
    private events: Events,
    private historyService: HistoryService,
    private toast: ToastController
  ) {}

  public histories: Array<any> = [];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  ngOnInit() {
    this.events.publish("has_menu", true);
    this.events.publish("has_header", true);
    this.init();
  }

  async init() {
    let result: any = await this.historyService.Listing();
    if (!result.error && result.data) {
      this.histories = result.data;
      this.setPage(1);
    } else {
      // this.toastMessageError(result.message);
    }
    console.log(result.data);
  }

  async toastMessageSuccess(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: "success"
    });
    return toast.present();
  }
  async toastMessageError(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: "danger"
    });
    return toast.present();
  }

  setPage(page: number) {
    
    // get pager object from service
    this.pager = this.pagerService.getPager(this.histories.length, page);

    // get current page of items
    this.pagedItems = this.histories.slice(
      this.pager.startIndex,
      this.pager.endIndex + 1
    );
  }

  formatTime(date: string) {
    return moment(new Date(date)).format("MMM DD, YYYY h:mm a");
  }
}
