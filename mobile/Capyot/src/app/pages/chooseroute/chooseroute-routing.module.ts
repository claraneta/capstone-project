import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooseroutePage } from './chooseroute.page';

const routes: Routes = [
  {
    path: '',
    component: ChooseroutePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChooseroutePageRoutingModule {}
