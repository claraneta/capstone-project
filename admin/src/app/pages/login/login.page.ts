import { Component, OnInit } from '@angular/core';
import { NavController, Events, ToastController } from '@ionic/angular';
import { Auth, Config } from '918kiss2u-sdk'
import { Storage } from '@ionic/storage';
import { AuthService } from '../../services/auth.service';
import { environment } from '../../../environments/environment'
@Component({
  selector: 'login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private nav: NavController
    , private events: Events
    , private storage: Storage
    , private toast: ToastController
    , private auth: AuthService) { }

  public username: any
  public password: any

  ngOnInit() {
    this.checkToken()
    this.events.publish("has_menu", false);
    this.events.publish("has_header", false);
  }

 async checkToken(){
    let token = await this.storage.get("token")
    if(token){
      this.nav.navigateRoot("/dashboard");
    }else{
      this.nav.navigateRoot("/login");
    }
  }

  async login() {
    if(this.username != undefined && this.password != undefined){
      let result: any = await this.auth.Login(this.username, this.password)
      if (!result.error && result.data) {
        Config.TOKEN = result.data.token.token
        await this.storage.set("token", result.data.token.token);
        await this.storage.set("username", result.data.username);
        await this.storage.set("url", result.data.url);
        this.toastMessageSuccess(result.message)
        this.nav.navigateRoot("/dashboard");
      } else {
        this.toastMessageError(result.message)
      }
    }
    else {
      this.toastMessageError("Please fill in all fields")
    }
  
  }
  async register() {
    this.nav.navigateRoot("/register");
  }

  async toastMessageSuccess(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: 'success',
    });
    return toast.present();
  }
  async toastMessageError(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: 'danger',
    });
    return toast.present();
  }

}