import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {  NbSelectModule, NbCardModule, NbButtonModule, NbIconModule, NbCheckboxModule, NbInputModule, NbDatepickerModule, NbTooltipModule, NbUserModule, NbListModule } from '@nebular/theme';

import { IonicModule } from '@ionic/angular';

import { RegisterPageRoutingModule } from './register-routing.module';

import { RegisterPage } from './register.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NbCardModule,
    NbSelectModule,
    NbButtonModule,
    NbIconModule,
    NbCheckboxModule,
    NbInputModule,
    NbDatepickerModule,
    NbTooltipModule,
    NbUserModule,
    NbListModule,
    RegisterPageRoutingModule
  ],
  declarations: [RegisterPage]
})
export class RegisterPageModule {}
