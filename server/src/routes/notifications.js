import Notifications from '../modules/notifications'

export const NotificationRoutes = [
    {
        method: 'POST',
        path: '/api/notification/save',
        config: {
            auth: 'simple'
        },
        handler: async (req, res) => {
            let message = req.payload.message
            let data = {
                _id: req.payload.id,
                message: message
            }
            let notification = await Notifications.Save(data)

            if (notification) {
                return {
                    error: 0,
                    data: notification,
                    message: 'Notification successfully saved'
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Saving notification failed'
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/notification/listing',
        config: {
            auth: 'simple'
        },
        handler: async (req, res) => {
            let notifications = await Notifications.NotificationsListing()
            if (notifications) {
                return {
                    error: 0,
                    data: notifications,
                    message: 'Listing success'
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: "Server could not fetch notifications from servers"
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/notification/get/{id}',
        config: {
            auth: 'simple'
        },
        handler: async (req, res) => {
            let notification = await Notifications.GetNotificationById(req.params.id)
            if (notification) {
                return {
                    error: 0,
                    data: notification,
                    message: 'Successfully getting the notification'
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Can not find notification'
                }
            }
        }
    },
]