import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import {  NbSelectModule, NbCardModule, NbButtonModule, NbIconModule, NbCheckboxModule, NbInputModule, NbDatepickerModule, NbTooltipModule, NbUserModule, NbListModule } from '@nebular/theme';

import { ProfilePageRoutingModule } from './profile-routing.module';

import { ProfilePage } from './profile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilePageRoutingModule,
    NbCardModule,
    NbSelectModule,
    NbButtonModule,
    NbIconModule,
    NbCheckboxModule,
    NbInputModule,
    NbDatepickerModule,
    NbTooltipModule,
    NbUserModule,
    NbListModule,
  ],
  declarations: [ProfilePage]
})
export class ProfilePageModule {}
