import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'loading', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)},
  {
    path: 'loading',
    loadChildren: () => import('./pages/loading/loading.module').then( m => m.LoadingPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'chooseroute',
    loadChildren: () => import('./pages/chooseroute/chooseroute.module').then( m => m.ChooseroutePageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./pages/notification/notification.module').then( m => m.NotificationPageModule)
  },
  {
    path: 'send-otp',
    loadChildren: () => import('./pages/send-otp/send-otp.module').then( m => m.SendOtpPageModule)
  },
  {
    path: 'testing',
    loadChildren: () => import('./pages/testing/testing.module').then( m => m.TestingPageModule)
  },
  {
    path: 'view-notification/:id',
    loadChildren: () => import('./pages/view-notification/view-notification.module').then( m => m.ViewNotificationPageModule)
  },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
