import { Component, NgZone } from "@angular/core";
import { NavController, ToastController } from "@ionic/angular";
import { AuthService } from "../../services/auth.service";
import {} from "googlemaps";
import { MapsAPILoader } from "@agm/core";
import { FormControl } from "@angular/forms";
import { GeolocationService } from "../../services/geolocation.service";
import { AutocompleteService } from "../../services/autocomplete.service";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  public autocompleteItems: any;
  public autocomplete: any;
  public acService: any;
  public placesService: any;
  public error: any;

  constructor(
    public zone: NgZone,
    private toast: ToastController,
    private mapsApiLoader: MapsAPILoader,
    private geolocationService: GeolocationService,
    private autocompleteService: AutocompleteService
  ) {}

  public searchControl: FormControl;

  public lat: any;
  public lng: any;
  public zoom: any = 16;
  public location: any;
  public title: string;
  public latitude: any;
  public longitude: any;
  public place: any;

  ngOnInit() {
    this.init();
    this.acService = new google.maps.places.AutocompleteService();
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ""
    };
  }

  async init() {
    let user_location: any = await this.geolocationService.GetCurrentLocation();
    console.log(user_location)
    if (user_location) {
      this.lat = user_location.location.lat;
      this.lng = user_location.location.lng;
    }
  }

  //newAutoComplete
  async search() {
    let response: any = await this.autocompleteService.SearchAutoComplete(
      this.place
    );
    console.log(response.status);
    console.log(JSON.parse(response.data)); // JSON data returned by server
    console.log(response.headers);
  }

  // AutoComplete Places
  async Search() {
    if (this.autocomplete.query == "") {
      this.autocompleteItems = [];
      return;
    }
    let self = this;
    let config = {
      types: ["address"], // other types available in the API: 'establishment', 'regions'
      input: this.autocomplete.query,
      componentRestrictions: { country: "ph" }
    };
    this.acService.getPlacePredictions(config, function(predictions, status) {
      console.log(status);
      if (status === "OK") {
        self.autocompleteItems = [];
        predictions.forEach(function(prediction) {
          self.autocompleteItems.push(prediction);
        });
      } else {
        self.error = status;
      }
    });

    if (this.error) {
      this.toastMessageError(this.error);
      this.error = "";
    } else {
      this.error = "";
    }
  }

  // Get latitude and longitude
  forwardGeocode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ address: address.description }, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
        this.zone.run(() => {
          this.latitude = results[0].geometry.location.lat();
          this.longitude = results[0].geometry.location.lng();
          console.log(this.latitude);
          console.log(this.longitude);
        });
      } else {
        alert("Error - " + results + " & Status - " + status);
      }
    });
  }

  async toastMessageError(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: "danger"
    });
    return toast.present();
  }
  async toastMessageSuccess(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: "success"
    });
    return toast.present();
  }

  //cris code for autocomplete
}
