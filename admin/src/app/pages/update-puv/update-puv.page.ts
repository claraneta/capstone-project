import { Component, OnInit, ViewChild } from "@angular/core";
import { Events, NavController, ToastController } from "@ionic/angular";
import { VehicleService } from "../../services/vehicle.service";
import { RouteService } from "../../services/route.service";
import { ActivatedRoute } from "@angular/router";
import { GoogleMap, MapInfoWindow, MapPolyline } from "@angular/google-maps";
import { findWhere } from "underscore";
import { HistoryService } from '../../services/history.service';

@Component({
  selector: "app-update-puv",
  templateUrl: "./update-puv.page.html",
  styleUrls: ["./update-puv.page.scss"]
})
export class UpdatePuvPage implements OnInit {
  @ViewChild(GoogleMap, { static: false }) map: GoogleMap;
  @ViewChild(MapInfoWindow, { static: false }) infoWindow: MapInfoWindow;
  @ViewChild(MapPolyline, { static: false }) poly: MapPolyline;
  constructor(
    private events: Events,
    private nav: NavController,
    private vehicleService: VehicleService,
    private routeService: RouteService,
    private activatedRoute: ActivatedRoute,
    private toast: ToastController,
    private historyService : HistoryService
  ) { }

  today: number = Date.now();
  public add_route: Array<any> = [];
  public show = true
  public isDisabled = true
  public type: any;
  public code: any;
  public routes: Array<any> = [];
  public vehicle_routes: any
  public status: any;
  public selected_routes: Array<any> = [];
  public vehicles: Array<any> = [];
  public marker_positions: google.maps.LatLngLiteral[] = [];
  public marker_options = { draggable: false, title: "" };
  public geocoder = new google.maps.Geocoder();
  public paths: google.maps.MVCArray<google.maps.LatLngLiteral[]>;
  public poly_options = {
    strokeColor: "#FF0000",
    strokeOpacity: 1.0,
    strokeWeight: 3,
    map: this.map
  };
  ngOnInit() {
    setTimeout(() => {
      this.initMapConfiguration();
    });
    this.events.publish("has_menu", true);
    this.events.publish("has_header", true);
    this.init();
    this.initVehicles(),
    setInterval(() => {
      this.today = Date.now();
    }, 1);
  }

  async initMapConfiguration() {
    if (navigator) {
      navigator.geolocation.getCurrentPosition(pos => {
        let location = {
          latitude: pos.coords.latitude,
          longitude: pos.coords.longitude
        };
        let position = {
          lat: location.latitude,
          lng: location.longitude
        };
        this.map.options = {
          mapTypeControl: true,
          scaleControl: true,
          zoomControl: true,
          fullscreenControl: false,
          streetViewControl: false,
          zoom: 15
        };
        this.map.center = { ...position };
      });
    }
  }


  async init() {
    let id = this.activatedRoute.snapshot.params.id;
    let result: any = await this.vehicleService.GetById(id);
    if (!result.error && result.data) {
      if (result.data.type === 'Bus') {
        this.show = false
      }
      else {
        this.show = true
        this.isDisabled = false

      }
      this.code = result.data.code;
      this.type = result.data.type;
      this.vehicle_routes = result.data.routes;
      this.status = result.data.update_status;
    } else {
      this.toastMessageError("Could not load vehicle");
    }
  }

  async toastMessageSuccess(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: 'success',
    });
    return toast.present();
  }
  async toastMessageError(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: 'danger',
    });
    return toast.present();
  }
  async initVehicles() {
    let result: any = await this.vehicleService.Listing();
    this.vehicles = result.data;
  }
  async Submit() {
    this.code = this.code.toUpperCase();
    let _code: any = findWhere(this.vehicles, { code: this.code }); // check if code already exits
    let id = this.activatedRoute.snapshot.params.id;
    let _routes: Array<any> = [];
    if (this.validateFields() && _code == undefined) {
      if (this.marker_positions.length > 9) {
        for (let index = 0; index < this.add_route.length; index++) {
          let element = this.add_route[index];
          let result: any = await this.routeService.Save(element[0]);
          if (!result.error && result.data) {
            _routes.push(result.data._id);
          } else {
            this.toastMessageError(result.message);
          }
        }
          if (_routes.length > 0) {
            if (this.type === 'Bus') {
              this.code = ''
            }
            this.saveVehicle(_routes, id)
          }
          console.log("Done");
   
      } else {
        if (this.marker_positions.length === 0) {
          if (this.type === 'Bus') {
            this.code = ''
          }
          this.saveVehicle(this.vehicle_routes, id)
        } else {
          this.toastMessageError("Please set the route of vehicle at least ten marker positions");
        }
      }
    } else if (this.validateFields() && _code.code === this.code) {
      let id = this.activatedRoute.snapshot.params.id;
      let vehicle = findWhere(this.vehicles, { _id: id })
      if (vehicle.code === this.code) {
        if (this.marker_positions.length > 9) {
            for (let index = 0; index < this.add_route.length; index++) {
              let element = this.add_route[index];
              let result: any = await this.routeService.Save(element[0]);
              if (!result.error && result.data) {
                _routes.push(result.data._id);
              } else {
                this.toastMessageError(result.message);
              }
            }
            if (_routes.length > 0) {
              this.saveVehicle(_routes, id)
            }
            console.log("Done");
        
        } else {
          if (this.marker_positions.length === 0) {
            this.saveVehicle(this.vehicle_routes, id)
          } else {
            this.toastMessageError("Please set the route of vehicle at least ten marker positions");
          }
        }
      } else {
        this.code = ''
        this.toastMessageError('Jeepney code already exists')
      }
    } else {
      this.toastMessageError("Please check carefully all fields");
    }
  }

  async addMarker(event) {
    let position = event.latLng.toJSON();
    this.marker_positions.push(position);
    this.marker_options.title = "Route #" + this.marker_positions.length;
    let route: any;
    let _landmark: Array<any> = [];
    let _routes: Array<any> = [];
    this.geocoder.geocode({ location: event.latLng.toJSON() }, function (
      results,
      status
    ) {
      if (status === "OK") {
        if (results[0]) {
          route = event.latLng.toJSON()
          route.landmark = results[0].formatted_address
          let landmark = results[0].formatted_address
          _landmark.push({ landmark })
          _routes.push(route);
        } else {
          window.alert("No results found");
        }
      } else {
        window.alert("Geocoder failed due to: " + status);
      }
    });
    this.add_route.push(_routes)   
    this.drawRoute(event.latLng);
  }

  async saveVehicle(routes, id: any) {
    if (this.type === 'Bus') {
      this.code = ''
     let data = {
        id: id,
        code: this.code,
        type: this.type,
        routes: routes,
        update_status: this.status
      };
      let result: any = await this.vehicleService.Save(data);
      if (!result.error && result.data) {
        this.toastMessageSuccess("Vehicle successfully updated");
        let _data = {
          action_type : 'Update-PUV',
          message : `Bus Vehicle successfully updated`,
          date: this.today
        }
        let history = await this.historyService.Save(_data)
        this.nav.navigateRoot("puv");
      } else {
        this.toastMessageError("Could not save vehicle");
      }
    } else {
      let letters = /^[A-Za-z]+$/;
      if (this.code.length === 3 && !isNaN(parseInt(this.code[0])) && !isNaN(parseInt(this.code[1])) && this.code[2].match(letters)) {
       let data = {
          id: id,
          code: this.code,
          type: this.type,
          routes: routes,
          update_status: this.status
        };
        let result: any = await this.vehicleService.Save(data);
        if (!result.error && result.data) {
          this.toastMessageSuccess("Vehicle successfully updated");
          let _data = {
            action_type : 'Update-PUV',
            message : `${data.code} Vehicle successfully updated`,
            date: this.today
          }
          let history = await this.historyService.Save(_data)
          this.nav.navigateRoot("puv");
        } else {
          this.toastMessageError("Could not save vehicle");
        }
      } else {
        this.code = "";
        this.toastMessageError("Invalid jeepney code");
      }
    }
  }
  async asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }

  // removeLastMarker() {
  //   this.marker_positions.pop();
  //   this.poly.getPath().clear();
  // }
  removeMarkers() {
    if(this.marker_positions.length == 0){
      this.toastMessageError("Nothing to clear")
    }else {
      this.poly.getPath().clear();
      this.marker_positions.length = 0
    }
}

  drawRoute(latlng) {
    const service = new google.maps.DirectionsService();
    const path = this.poly.getPath();
    let self = this;
    if (path.getLength() === 0) {
      path.push(latlng);
    } else {
      service.route(
        {
          origin: path.getAt(path.getLength() - 1),
          destination: latlng,
          travelMode: google.maps.TravelMode.DRIVING
        },

        function (result, status) {
          if (status == google.maps.DirectionsStatus.OK) {
            for (
              var i = 0, len = result.routes[0].overview_path.length;
              i < len;
              i++
            ) {
              path.push(result.routes[0].overview_path[i]);
              // this.paths = path.push(result.routes[0].overview_path[i]);
            }
          }
        }
      );
    }
  }

  clearAllFields() {
    this.type = "";
    this.code = "";
    this.routes = [];
    this.status = "";
  }

  validateFields() {
    if (this.type === 'Bus' && this.status) {
      return true;
    }
    else if (this.type === 'Jeep' && this.code != '') {
      return true;
    }
    else {
      return false;
    }
  }
}