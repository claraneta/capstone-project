import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddPuvPage } from './add-puv.page';

const routes: Routes = [
  {
    path: '',
    component: AddPuvPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddPuvPageRoutingModule {}
