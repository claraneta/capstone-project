// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API_URL: "http://localhost:8080",
  TOKEN: "",
  USER: "",
  firebase: {
    apiKey: "AIzaSyAsGAR_Bj_6-7Xb9y339Jbq5Wza4rrl2Wk",
    authDomain: "capyot-5b943.firebaseapp.com",
    databaseURL: "https://capyot-5b943.firebaseio.com",
    projectId: "capyot-5b943",
    storageBucket: "capyot-5b943.appspot.com",
    messagingSenderId: "273709406533",
    appId: "1:273709406533:web:26eb8118ad8d71c26c062c",
    measurementId: "G-62E8HMLQFF"
  },
  GOOGLE_MAPS_KEY: "AIzaSyDIvaE6mEMzIlYDAYvA9taHQCswV4JQRzc"
};

// apiKey: 'AIzaSyC9KBH7Irrb1yag4Z-iYvhtdYiERtlsayo',
// authDomain: 'capyot-10731.firebaseapp.com',
// databaseURL: 'https://capyot-10731.firebaseio.com',
// projectId: 'capyot-10731',
// storageBucket: 'capyot-10731.appspot.com',
// appId: '1:449821655392:web:a5e4af595da76450b45e05',
// measurementId: 'G-ELLBFKTMFD'

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
