import { Component, OnInit } from '@angular/core';
import { Events, NavController, ToastController } from "@ionic/angular";
import { UsersService } from '../../services/users.service'
import { findWhere } from "underscore";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  public username: any;
  public url: any;
  public id: any;
  public password: any;
  public _password: any;
  public confirm_password: any;
  public show = false;
  constructor(
    private events: Events,
    private usersService: UsersService,
    private toast: ToastController,
    private nav: NavController
  ) { }

  ngOnInit() {
    this.events.publish("has_menu", true);
    this.events.publish("has_header", true);
    this.init()
  }

  async init() {
    let result: any = await this.usersService.GetCurrent();
    if (!result.error && result.data) {
      this.id = result.data.owner._id
      this.username = result.data.owner.username,
      this.url = result.data.owner.url
    } else {
      this.toastMessageError(result.message);
    }
  }

  async save() {
    if (this.validateAllFields()) {
      if (this.password === this.confirm_password) {
        if (this.validatePassword(this.password)) {
          let data = {
            id: this.id,
            password: this.password,
            username: this.username,
            url: this.url
          }
          let result: any = await this.usersService.UpdateCurrent(data);
          this.toastMessageSuccess(result.message);
          this.nav.navigateRoot("dashboard");
        } else {
          this.toastMessageError("Password should at least 7 characters and have a number on it");
          this.password = "";
          this.confirm_password = "";
        }
      } else {
        this.toastMessageError("Password does not match");
        this.password = "";
        this.confirm_password = "";
      }
    } else {
      this.toastMessageError('Please populate all fields')
      this.clearAllFields()
    }
  
  }

  async toastMessageSuccess(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: "success"
    });
    return toast.present();
  }
  async toastMessageError(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: "danger"
    });
    return toast.present();
  }

  validatePassword(password) {
    if (password.length > 7) {
      if (this.isnumber(password)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  validateAllFields() {
    if (this.confirm_password && this.password) {
      return true;
    } else {
      return false;
    }
  }

  clearAllFields() {
    this.password = ""
    this.confirm_password = ""
  }

  isnumber(password: string) {
    var matches = password.match(/\d+/g);
    if (matches != null) {
      return true;
    } else {
      return false;
    }
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      }
    }
  }

}
